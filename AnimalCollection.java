import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AnimalCollection {
    public static List<Animal> animalList = new ArrayList<>();
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        menu();
    }

    public static void menu() {
        int choise;
        do {
            System.out.println("Spis zwierząt \n" +
                    "Menu \n" +
                    "1-Dodaj zwierze\n" +
                    "2-usuń zwieże \n" +
                    "3-Co robia zwierzeta\n" +
                    "4-Wyjście");
            choise = scanner.nextInt();
            switch (choise) {
                case 1:
                    addAnimal(scanner);
                    break;
                case 2:
                    remuveAnimal(scanner);
                    break;
                case 3:
                    moveAll(animalList);
                    break;
            }
        }while (choise != 4);
    }

    private static void addAnimal(Scanner scanner) {
        System.out.println("Jakie zwierze chcesz dodać kota czu psa\n" +
                "Wpisz poniżej");
        String animal = scanner.next();
        switch (animal) {
            case "Kot":
                String catName = scanner.next();
                Cat cat = new Cat(catName);
                animalList.add(cat);
                break;
            case "Pies":
                String dogName = scanner.next();
                Dog dog = new Dog(dogName);
                animalList.add(dog);
                break;
        }
        moveAll(animalList);
        scanner.next();
        return;
    }

    private static void remuveAnimal(Scanner scanner) {
        moveAll(animalList);
        int index = scanner.nextInt();
        animalList.remove(index);
        moveAll(animalList);
    }

    public static void moveAll(List<Animal> animalList) {
        for(Animal animal:animalList){
            animal.move();
        }
    }
}
